=========================================================================================================================================================
                                             CONCEPT GUIDE TO REACT JS COMPONENT SYSTEM DISCUSSIONS
============================================================================================================================================================

STEP 1 - use the toolchain with npx to install and execute packages needed to create the react-app

what is the change in "behavior" when using
npx (x) execute - hinahanap niya yung pinaka latest version ng dependency 
                  online. 
npm(node package manager) - this will only install the package version that
                            you have locally. 

SCRIPT: npx create-react-app <insert desired file name>
        npx create-react-app course_booking_b164

STEP 2 - Once the app has been successfully installed go inside the project 
question : what will happen if i run npm start outside the project? 

SCRIPT: cd <project name>

STEP 3 - serve the project, it should be running at the default port localhost:3000

SCRIPT: npm start 
note: it will execute automatic with the react logo presented at the browser. 

STEP 4 - install react-bootstrap via terminal command:

   *VERSION CORRECTION UPDATED 4/27/2022*
SCRIPT: npm install react-bootstrap@1.5.2 bootstrap@4.6.0

STEP 5: import bootstrap inside index.js 

SCRIPT: import 'bootstrap/dist/css/bootstrap.min.css'

STEP 6: Create a Components and Pages subfolder inside the src folder

REASON: we want to have a seperation of concerns, so we divided the subcomponents and the main components 
of our project 

ANOTHER REASON: remember that one of the advantages of using a React Library is that the components are 
reusable, meaning we can call on a component to appear over and over again without having to code the scripts
again, ..in a sense we created 2 different folders to properly organize the components that we would
like to call on across our project. 

STEP 7: refactor the navbar into the components folder as a single, reusable component named NavBar.js

STEP 8: define navbar and render it at the root div in index.js

STEP 9: import the NavBar component in the App.js entry point

STEP 10: import App.js into the index.js to render it at the root div

STEP 11: create a Banner component inside the components folder 

STEP 11: import Banner component in App.js and render it inside a
        bootstrap container underneath the NavBar component

STEP 12: Create a Highlights component inside components subfolder. 

STEP 13: import Highlights component in App.js and render it inside a bootstrap container
         together with the NavBar component. 
         
STEP 14: clear the CSS in App.css and set the card height to 100%
Send a screenshot in Hangouts of your modified App.CSS
STEP 15: refactor the Banner and Highlights components into a page named Home.js

STEP 16: Install react-router-dom package to implement routing in our React app.


STEP 17: In the App component, import the BrowserRouter component from react-router-dom 
(you can give it an alias of Router for convenience / familiarity). //this is an optional step
Wrap the App component contents within the Router component to enable routing within the app.


STEP 18: Still in the App component, import the Route component from react-router-dom to set which 
components will be rendered for which endpoints.


STEP 19: In the Navbar component, set the correct URL endpoints as the values for their respective 
href JSX attributes.
=============================================================================================================================================================

STEP 20: Push in a remote repo in gitlab 
    PROJECT NAME: course_booking_react
    COMMIT MESSAGE: Completed day 1 Manual for React Session

==============================================================================================================================================================
END OF NOTES



      
 