Note: If you have already generated your SSH Key and saved it to Gitlab, no need to do any of this anymore.

1. Generate an SSH Key:

  a. In your terminal, type:
  ssh-keygen -t ed25519 -C "email@example.com"
  
  (Replace email@example.com with your own email, but remember to keep the quotation marks)
  
  b. Various messages will display, simply keep pressing enter until you see a message containing "The key's randomart image is"
  At that point, the key has been generated.

2. Copy your SSH Key:

  In your terminal, type one of the following:
  
  (FOR MAC): pbcopy < ~/.ssh/id_ed25519.pub
  (FOR LINUX): xclip -sel clip < ~/.ssh/id_ed25519.pub
  (FOR WINDOWS): cat ~/.ssh/id_ed25519.pub | clip

  Your SSH Key is now saved in your clipboard. Unless you copy some other line of text, it will remain there. To be safe, immediately open Notepad (or a
  similar app) and paste your key there. It should look similar (but not exactly the same) to this:

  ssh-ed25519 l1SZD49ADLFFFII1NzaCdN8A/SkfTjld9nJ3nu4Yd5Y8g8+Km11/N+ZZMh4shQ email@example.com

  (Note: The above is only an example and is an invalid SSH Key. Please do NOT use it)

3. Apply your SSH Key to Gitlab:

  a. Go to https://gitlab.com/profile/keys and ensure you are logged into an account with the same email you used for step 1 of this guide.
  
  b. Right click on the box with the gray text that says [Typically starts with "ssh-ed25519 ..." or "ssh-rsa ..."] and paste your SSH Key there.
  
  c. Click on the green "Add key" button below the bow.
  
  d. If you can now see something under "Your SSH keys" near the bottom of the page, you're done!